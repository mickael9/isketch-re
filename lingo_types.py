import struct
import datetime

__all__ = ['TYPES', 'LValue', 'LBase', 'LVoid', 'LUnknown',
           'LInteger', 'LFloat', 'LString', 'LSymbol',
           'LStringList', 'LList', 'LPropList', 'LPoint', 'LRect',
           'LColor', 'LDate', 'LMedia', 'LPicture',
           'L3dVector', 'L3dTransform',
           'MessageHeader']


class LValue:
    level = 0

    def __init__(self, st):
        self.msg('%s: construct (offset=%d, data=%r...)' % (
            self.__class__.__name__, st.tell(),
            st.getvalue()[st.tell():st.tell()+40]))
        self.st = st
        LValue.level += 1
        self.value = self._value()
        LValue.level -= 1
        self.msg('Value: %r' % (self.value,))

    def msg(self, text):
        return
        print(LValue.level * "| " + text)

    def read(self, n=None):
        rd = self.st.read(n)
        self.msg('read: %r' % rd)
        if n is not None:
            assert len(rd) == n
        return rd

    def read_short(self):
        return struct.unpack('>h', self.read(2))[0]

    def read_int(self):
        return struct.unpack('>i', self.read(4))[0]

    def read_double(self):
        return struct.unpack('>d', self.read(8))[0]

    def _value(self):
        type_id = self.read_short()
        return TYPES.get(type_id, LUnknown)(self.st).value


class LBase(LValue):
    def _value(self):
        raise NotImplementedError()

    def __repr__(self):
        return '%s(%r)' % (self.__class__.__name__, self.value)


class LVoid(LBase):
    def _value(self):
        return self

    def __repr__(self):
        return "LVoid()"


class LUnknown(LBase):
    def _value(self):
        self.st.seek(self.st.tell() - 2)  # include type byte
        self.data = self.st.read()
        return self

    def __repr__(self):
        return "LUnknown(data=%r)" % self.data


class LInteger(LBase):
    def _value(self):
        return self.read_int()


class LFloat(LBase):
    def _value(self):
        return self.read_double()


class LString(LBase):
    def _value(self):
        length = self.read_int()
        ret = self.read(length).decode('utf-8')
        if length % 2:
            try:
                self.read(1)
            except AssertionError:
                pass  # Sometimes, the padding is not present
        return ret


class LSymbol(LString):
    def _value(self):
        return "#" + LString._value(self)


class LStringList(LBase):
    def _value(self):
        count = self.read_int()
        return tuple(LString(self.st).value for i in range(count))


class LList(LBase):
    def _value(self):
        return tuple(LValue(self.st).value
                     for i in range(self.read_int()))


class LPropList(LBase):
    def _value(self):
        return dict((LValue(self.st).value, LValue(self.st).value)
                    for i in range(self.read_int()))


class LPoint(LBase):
    def _value(self):
        self.x, self.y = (LValue(self.st).value for i in range(2))
        return self

    def __repr__(self):
        return "LPoint(x=%r, y=%r)" % (self.x, self.y)


class LRect(LBase):
    def _value(self):
        self.x, self.y, self.w, self.h = (LValue(self.st).value
                                          for i in range(4))
        return self

    def __repr__(self):
        return "LRect(x=%d, y=%d, w=%d, h=%d)" % (
                self.x, self.y, self.w, self.h)


class LColor(LBase):
    def _value(self):
        self.color = self.read_int()
        return self

    def __repr__(self):
        return "LColor(#%08x)" % self.color


class LDate(LBase):
    def _value(self):
        return datetime.datetime.fromtimestamp(
                self.read_int() + (self.read_int() << 32))


class LMedia(LBase):
    def _value(self):
        size = self.read_int()
        self.data = self.read(size)
        if size % 2:
            self.read(1)
        return self

    def __repr__(self):
        return "%s(...)" % (self.__class__.__name__)


class LPicture(LMedia):
    pass


class L3dVector(LBase):
    def _value(self):
        self.floats = [self.read_double() for i in range(3)]
        return self

    def __repr__(self):
        return "%s(%r)" % (self.__class__.__name__, self.floats)


class L3dTransform(LBase):
    def _value(self):
        self.floats = [self.read_double() for i in range(16)]
        return self

    def __repr__(self):
        return "%s(%r)" % (self.__class__.__name__, self.floats)


class MessageHeader(LBase):
    def _value(self):
        assert self.read(2) == b'r\x00'
        self.data_size = self.read_int()
        self.errCode = self.read_int()
        self.timestamp = self.read_int()
        self.subject = LString(self.st).value
        self.senderID = LString(self.st).value
        self.recptID = LStringList(self.st).value
        return self

    def __repr__(self):
        return ('MessageHeader(err=%d, timestamp=%d, subject=%s, sender=%s, recpt=%r)' %
                (self.errCode, self.timestamp, self.subject, self.senderID, self.recptID))


TYPES = {
    0: LVoid,
    1: LInteger,
    2: LSymbol,
    3: LString,
    5: LPicture,
    6: LFloat,
    7: LList,
    8: LPoint,
    9: LRect,
    10: LPropList,
    18: LColor,
    19: LDate,
    20: LMedia,
    22: L3dVector,
    23: L3dTransform,
}
