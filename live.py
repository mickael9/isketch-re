#!/usr/bin/env python

import sys
import pprint
from scapy.all import *
from io import BytesIO

from lingo_types import *
from blowfish_smus import Cipher

KEY = b'#Alle,qzEeKalD0e0hlw'


class Framer:
    def __init__(self):
        self.cipher = Cipher(KEY)
        self.pending = bytearray()
        self.length = 0

    def decrypt(self, data):
        size = len(data)
        if size % 8:
            data += b' ' * (8 - (size % 8))
        return self.cipher.decrypt_smus(data)[:size]

    def new_data(self, data):
        #print("new_data: %r" % data)
        self.pending.extend(data)

    def read_one_frame(self):
        if not self.length:
            if len(self.pending) >= 8:
                decrypted_header = self.decrypt(self.pending[:8])
                assert decrypted_header[:2] == b'r\x00'
                self.length = struct.unpack('>i', decrypted_header[2:6])[0] + 6

        if self.length and len(self.pending) >= self.length:
            ret = BytesIO(self.decrypt(self.pending[:self.length]))
            del self.pending[:self.length]
            self.length = 0
            return ret


def handle_packet(p):
    ip = p.getlayer(IP)
    try:
        data = ip.load
    except AttributeError:
        return

    if ip.dport == 1626:
        out_framer.new_data(data)
    elif ip.sport == 1626:
        in_framer.new_data(data)

    for name, framer in ('out', out_framer), ('in', in_framer):
        while True:
            frame = framer.read_one_frame()
            if not frame:
                break
            #print('Frame: %r' % frame.getvalue())
            header = MessageHeader(frame)
            print('[%s] %s --> %s' % (header.subject, header.senderID, ', '.join(header.recptID)))
            if header.subject == 'Logon' and header.recptID == ('System',):
                # For some reason, the logon message is double encrypted
                data_size = header.data_size
                frame = BytesIO(framer.decrypt(frame.read(data_size)))

            msg = LValue(frame).value
            pprint.pprint(msg)
            if frame.tell() != len(frame.getvalue()):
                print('Trailing data: %r' % frame.read())
            print("*" * 100)


if __name__ == '__main__':
    in_framer = Framer()
    out_framer = Framer()
    pcap_file = None
    if len(sys.argv) > 1:
        pcap_file = sys.argv[1]

    sniff(store=False, prn=handle_packet, filter='tcp port 1626', offline=pcap_file)
